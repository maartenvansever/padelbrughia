<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164840027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
   
  gtag('config', 'UA-164840027-1');
</script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content= "width=device-width, user-scalable=no">
    <link rel="icon" href="images/Logo.JPG" type="image/gif" sizes="20x20">
    <title>Padelbrughia</title>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/screen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/club.css">
</head>

<body>
    <header>
        <nav>
            <h1>PadelBrughia</h1>
            <div id="arrowDown" class="closeArrow openArrow rotateimg"></div>
            <div id="popUpNav" class="closeNav openNav">
                <ul>
                    <li><a href="index.html">Startpagina</a></li>
                    <li><a href="club.php">Club</a></li>
                    <li><a href="academy.php">Academy</a></li>
                    <li><a href="reserveren.php">Reserveren</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="partners.php">Partners</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <h1>Club</h1>
        <article id="padel">
            <h2>Wat is padel?</h2>
            <img src="images/WPadel.jpg" title="padel" alt="padel">
            <p>Padel is een racketsport met ingrediënten uit o.a. tennis en squash.
                Het is een terugslagspel dat zowel enkel als dubbel wordt gespeeld op een rechthoekig terrein omringd door U-vormige glazen of betonnen wanden.
                De bal blijft in het spel na contact met de wanden. Dit zorgt voor langere rally’s en levert meer spelplezier op. Strategie is bij padel belangrijker dan kracht!
                <br />
                <a href="https://www.tennisvlaanderen.be/wat-is-padel"> Klik hier voor meer info</a>

            </p>
            <p>
                Padel is toegankelijk voor alle leeftijden en is erg aantrekkelijk door zijn sociaal karakter.
                Een ideale sport om te spelen met vrienden, familie of collega’s. Padel is plezierig en uitdagend voor sportievelingen van eender welk niveau!
            </p>
        </article>
       
        <article id="Accommodatie">
            <h2>Accommodatie</h2>
            <img src="images/Accomodatie.jpg">
        </article>
        <article>
            <h2> Tarieven</h2>
            <h3>Abonnementen</h3>
            <table>
                <thead>
                <tr >
                    <td></td>
                    <td class="bold">Jaar abbonement</td>
                    <td class="bold">Seizoenen Abonnement</td>
                </tr>
                </thead>
                <tbody>
                    <tr class="borderbottom">
                        <td class="bold">Lid volwassen +25 jaar</td>
                        <td>220 EUR</td>
                        <td>125 EUR</td>
                    </tr >
                    <tr class="borderbottom">
                        <td class="bold">Lid volwassenen -25 jaar</td>
                        <td>170 EUR</td>
                        <td>100 EUR</td>
                    </tr>
                    <tr class="borderbottom">
                        <td class="bold">Lid -18 jaar</td>
                        <td>120 EUR</td>
                        <td>70 EUR</td>
                    </tr  >
                    <tr class="borderbottom">
                        <td class="bold">Lid -12 jaar</td>
                        <td>80 EUR</td>
                        <td>50 EUR</td>
                    </tr>
                    <tr>
                        <td class="bold">Famillie Abonnement</td>
                        <td>550 EUR</td>
                    </tr>
                </tbody>
            </table>
            <h3>Verhuring</h3>
            <table>
                <tbody>
                <tr class="borderbottom">
                    <td class="bold">Court huren (1,5u)</td>
                    <td>40 EUR</td>
                </tr>

                <tr>
                    <td class="bold">Racket huren</td>
                    <td>2 EUR</td>
                </tr>
                </tbody>
            </table>
        </article>
        <article>
            <h3>Interclub</h3>
            <img src="images/Interclub.jpg" alt="interclub" title="interclub">
            <p>Met Padelbrughia nemen wij deel aan de padelinterclub. Dit is een competitie tussen verschillende padelclubs in Vlaanderen.
            Er worden telkens 6 wedstrijden gespeeld door een groep van 4-6 spelers.
            Er zijn per reeks ongeveer 4 tot 6 ontmoetingen en deze worden gespeeld in het weekend.</p>
            <p id="contactinterclub">Interesse om deel te nemen?</br>
                Hebt u nog geen groepje of hebt u al een team samengesteld?</br>
             Contacteer ons nu <a href="mailto:info@padelbrughia.be">info@padelbrughia.be</a>
            </p>
        </article>
        <!--<a href="#arrowDown" id="backToTop" class="rotateimg hidden"><p>Top</p></a>-->
        <!-- Load Facebook SDK for JavaScript -->
     <!-- <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code --><!--
      <div class="fb-customerchat"
        attribution=install_email
        page_id="1843804475749158"
  logged_in_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ..."
  logged_out_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ...">
      </div>-->
    </main>
    <?php
      include('templates/footer.php');
      ?>
    <div id="webdream">
        <p>Made by Webdream</p>
    </div>
</body>
<script src="assets/js/global.js"></script>
<script src="assets/js/nav.js"></script>
<script src="assets/js/toTopButton.js"></script>
</html>