'use strict';

document.addEventListener('DOMContentLoaded', init);

function init() {
    if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js").then(function (r) {
            console.info('Service worker registered successfully', r)
        }).catch(function (err) {
            console.info('Service worker registration failed: ', err)
        });
    }
}