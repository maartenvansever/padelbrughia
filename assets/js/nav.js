document.addEventListener('DOMContentLoaded', init);

function init() {
    document.getElementById("arrowDown").addEventListener("click", nav);
}


function nav(e) {
    e.preventDefault();
    if (!e.target.classList.contains("openArrow")){
        document.getElementById("arrowDown").classList.add("openArrow");
        document.getElementById("popUpNav").classList.add("openNav");
        document.getElementById("arrowDown").classList.add("rotateimg");
    } else {
        document.getElementById("arrowDown").classList.remove("openArrow");
        document.getElementById("popUpNav").classList.remove("openNav");
        document.getElementById("arrowDown").classList.remove("rotateimg");
    }
}


