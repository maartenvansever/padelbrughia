<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164840027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164840027-1');
</script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content= "width=device-width, user-scalable=no">
    <link rel="icon" href="images/Logo.JPG" type="image/gif" sizes="20x20">
    <title>Padelbrughia</title>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/screen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reserveren.css">

</head>

<body>
    <header>

        <nav>
            <h1>PadelBrughia</h1>
            <div id="arrowDown" class="closeArrow openArrow rotateimg"></div>
            <div id="popUpNav" class="closeNav openNav">
                <ul>
                    <li><a href="index.html">Startpagina</a></li>
                    <li><a href="club.php">Club</a></li>
                    <li><a href="academy.php">Academy</a></li>
                    <li><a href="reserveren.php">Reserveren</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="partners.php">Partners</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <h1> reserveren</h1>
        <article id="old">
            <a href="http://reserveren.tcbrughia.be/reserveren" id="reserveren" onclick="javascript:void window.open('http://reserveren.tcbrughia.be/reserveren','1588091441141','width=1300,height=1000,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=-50,top=0');return false;"><button >Reservatie tot 31/03</button></a>
        </article>
        <article id="new">
            <a href="http://reserveren.tcbrughia.be/reserveren" id="reserverennew" onclick="javascript:void window.open('https://www.tennisvlaanderen.be/reserveer-een-terrein?clubId=1344151&ownClub=true&clubCourts%5b0%5d=I&clubCourts%5b1%5d=O','1588091441141','width=1300,height=1000,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1,left=-50,top=0');return false;"><button >Reserveer hier</button></a>
          
        </article>
        <article>
            <h2>House rules</h2>
            <div id="number1" class="rule">
              <div class="number2">
                <span class="numbers">1</span>
              </div>
                <span class="ruleText">Tennis Vlaanderen lidnummer nodig.</span>
                <span class="ruleText">Niet-leden kunnen ook reserveren bij ons door een account aan te maken bij Tennis Vlaanderen (zie knop reserveer hier).</span>
            </div>

            <div id="long" class="rule">
              <div id="longnumber" class="number2">
                <span class="numbers">2</span>
              </div>
                <span class="ruleText"> We maken een onderscheid tussen piek- en dalmomenten. </span>
                <span class="ruleText">PIEK: elke weekdag vanaf 17u en tijdens het weekend van 8u tot 17u. </span>
                <span class="ruleText"> DAL: elke weekdag van 8u tot 17u en tijdens het weekend vanaf 17u. </span>
            </div>

            <div class="rule">
              <div class="number2">
                <span class="numbers">3</span>
              </div>
                <span class="ruleText"> Je kan maximaal per lid 3 piekmomenten en 2 dalmomenten vastleggen. Vanaf het moment dat er 1 van je speelmomenten afgelopen is kan je opnieuw een moment vastleggen.</span>           
            </div>
            <div class="rule">
              <div class="number2">
                <span class="numbers">4</span>
              </div>
                <span class="ruleText"> Reserveren doe je voor een anderhalf uur (90 minuten), binnen de aangegeven tijdsloten.</span>           
            </div>

            <div class="rule">
              <div class="number2">
                <span class="numbers">5</span>
              </div>
                <span class="ruleText">Je kan geen 2 opeenvolgende reservaties dezelfde dag vastleggen. M.a.w. je kan geen 3u na elkaar vastleggen. </span>           
            </div>

            <div class="rule">
              <div class="number2">
                <span class="numbers">6</span>
              </div>
                <span class="ruleText">Er moeten 4 namen op de reservatietool staan. Indien er een niet-lid opstaat moet er voor die perso(o)n(en) direct online betaald worden.
            </div>
            <img src="images/rules.jpeg" alt="coronarules" title="rules" class="clicked">
       
        </article>
        <!-- Load Facebook SDK for JavaScript -->
     <!-- <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };
        let test = "";
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code --><!--
      <div class="fb-customerchat"
        attribution=install_email
        page_id="1843804475749158"
  logged_in_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ..."
  logged_out_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ...">
      </div>-->
    </main>
    <?php
      include('templates/footer.php');
      ?>

</body>
<script src="assets/js/global.js"></script>
<script src="assets/js/nav.js"></script>
<script src="assets/js/reservation.js"></script>
</html>
