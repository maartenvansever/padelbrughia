<!doctype html>
<html lang="nl">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164840027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164840027-1');
</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164840027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164840027-1');
</script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content= "width=device-width, user-scalable=no">
    <link rel="icon" href="images/Logo.JPG" type="image/gif" sizes="20x20">
    <title>Padelbrughia</title>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/screen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/contact.css">
</head>

<body>
    <header>

        <nav>
            <h1>PadelBrughia</h1>
            <div id="arrowDown" class="closeArrow openArrow rotateimg"></div>
            <div id="popUpNav" class="closeNav openNav">
                <ul>
                    <li><a href="index.html">Startpagina</a></li>
                    <li><a href="club.php">Club</a></li>
                    <li><a href="academy.php">Academy</a></li>
                    <li><a href="reserveren.php">Reserveren</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="partners.php">Partners</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <h1>Contact</h1>
        <div id="#contactForms">
            <section id="facebook">
                <i class="fa fa-facebook"></i>
                <h2>Facebook</h2>
                <a href="https://www.facebook.com/Padel-Brughia-1843804475749158/">Bezoek hier onze Facebook pagina</a>
            </section>
            <section id="mail">
                <i class="material-icons">email</i>
                <h2>Mailen</h2>
                <a href="mailto:info@padelbrughia.be">info@padelbrughia.be</a>
            </section>

            <section id="contactAddress">
                <i class="material-icons">location_on</i>
                <h2>Ons adres</h2>
                <a href="https://goo.gl/maps/oex9qmNSigx4eQvM6">Boogschutterslaan 37, 8310 Sint-Kruis (Brugge)</a>
            </section>
        </div>
<!-- Load Facebook SDK for JavaScript -->
   <!--   <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code --><!--
      <div class="fb-customerchat"
        attribution=install_email
        page_id="1843804475749158"
  logged_in_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ..."
  logged_out_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ...">
      </div>-->
    </main>
    <?php
      include('templates/footer.php');
      ?>
        <div id="webdream">
        <p>Made by Webdream</p>
    </div>
</body>
<script src="assets/js/global.js"></script>
<script src="assets/js/nav.js"></script>
</html>