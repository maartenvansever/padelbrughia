<footer>
        <div id="CA">
            <section id="contact">

                <h2>Contact</h2>
                    <div><i class="material-icons">email</i><a href="mailto:info@padelbrughia.be">info@padelbrughia.be</a></div>

            </section>

            <section id="address">
                <address>
                    <h2>Adres</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2499.157655077056!2d3.246156315964106!3d51.216171339775336!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c350b25494877f%3A0xde4dcedb47d41ff1!2sTenniscentrum%20Brughia!5e0!3m2!1snl!2sbe!4v1567451603117!5m2!1snl!2sbe"></iframe>
                <!-- <i class="material-icons">location_on</i>--><p style="color:white;">Boogschutterslaan 37, 8310 Sint-Kruis (Brugge)</p>
                </address>
            </section>
        </div>
        <section id="partners">
            <h2>Partners</h2>
            <div id="footerPartners">
                <img src="images/FooterLogoYeti.png" title = "Yeti" alt="Yeti">
                <img src="images/footerSerruyslogo.png" title="serruys" alt="serruys">
                <img src="images/footerLogo_gare_de_robe.png" title="gare de robe" alt="gare de robe">
                <img src="images/sponsorlogo.png" title="Zakenkantoor Maene" alt="Zakenkantoor Maene">
                <img src="images/Alexandra.png" title="Alexandra restaurant" alt="Alexandra restaurant">
                <img src="images/burger.png" title="burger" alt="burger" >
                <img src="images/LogoCar_Cost_Advisor_kleur.png" title="car cost advisor" alt="car cost advisor">
            </div>
        </section>
    </footer>