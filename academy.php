<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content= "width=device-width, user-scalable=no">
    <link rel="icon" href="images/Logo.JPG" type="image/gif" sizes="20x20">
    <title>Padelbrughia</title>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/screen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/academy.css">
</head>

<body>
    <header>

        <nav>
            <h1>PadelBrughia</h1>
            <div id="arrowDown" class="closeArrow openArrow rotateimg"></div>
            <div id="popUpNav" class="closeNav openNav">
                <ul>
                    <li><a href="index.html">Startpagina</a></li>
                    <li><a href="club.php">Club</a></li>
                    <li><a href="academy.php">Academy</a></li>
                    <li><a href="reserveren.php">Reserveren</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="partners.php">Partners</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <h1>Academy</h1>
        <p id="note">Alle activiteiten van onze academy worden georganiseerd door Padel & Move.</p>
        <article>
            <h3>Lessen</h3>
            <h4>jeugd + volwassen</h4>
            <table>
                <thead>
                <tr>
                    <td></td>
                    <td><b>1 les = 1 uur</b></td>
                </tr>
                </thead>
                <tbody>
                <tr class="underline">
                    <td><b>privé</b></td>
                    <td>35 eur</td>
                </tr>
                <tr class="underline">
                    <td><b>Groep van 2 pers</b></td>
                    <td>20 eur per persoon</td>               
                </tr>
                <tr class="underline">
                    <td><b>Groep van 3 pers</b></td>
                    <td>15 eur per persoon</td>            
                </tr>
                <tr class="underline">
                    <td><b>Groep van 4 pers</b></td>
                    <td>12.5 eur per persoon</td>       
                </tr>
                  
                </tbody>
                </tbody>
            </table>
           
        </article>
        <article>
                <h3>Groepen & teambuilding</h3>
                <!-- autoplay-->
                <video controls muted  loop>
                    <source src="video/teambuilding.mp4" type="video/mp4">
                </video>

                <p>Bent u op zoek naar een leuke teambuilding activiteit voor uw vrienden, familie, bedrijf, buren,…?
                    Zoek niet langer. PadelBrughia heeft voor u tal van mogelijkheden! Onderstaand een overzicht van wat wij voor u kunnen aanbieden:
                </p><br>
                <p>Racket Sports: onze locatie biedt u de mogelijkheid om een initiatie of competitie te houden in verschillende racketsporten (padel, tennis, badminton, squash, tafeltennis en whackitball).
                    U kunt uw eigen pakket samenstellen naargelang uw wensen (1 of meerdere sporten). Wij maken een activiteit op naar uw wens!</p><br>
                <p class="center">Voor meer info of offerte, contacteer ons via info@padelbrughia.be</p>
                <p class="center">Tot binnenkort @PadelBrughia.</p>
        </article>
        <article>

        </article>
        <!-- Load Facebook SDK for JavaScript -->
    <!-- <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code --><!--
      <div class="fb-customerchat"
        attribution=install_email
        page_id="1843804475749158"
  logged_in_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ..."
  logged_out_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ...">
      </div>-->
    </main>
    <?php
    include('templates/footer.php');
    ?>
        <div id="webdream">
        <p>Made by Webdream</p>
    </div>
</body>
<script src="assets/js/global.js"></script>
<script src="assets/js/nav.js"></script>
</html>