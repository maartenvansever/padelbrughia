"use strict";
const APP_OFFLINE_URL = "/offline.html";
const URLS_TO_CACHE = [
    "/",
    "/readme.md",
    //HTML
    "/academy.php",
    "/club.php",
    "/contact.php",
    "/index.php",
    "/partners.php",
    "/reserveren.php",
    // ICONS
    "/images/icons/android-icon-192x192-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-57x57-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-60x60-dunplab-manifest-32021.JPG", 
    "/images/icons/apple-icon-72x72-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-76x76-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-114x114-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-120x120-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-144x144-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-152x152-dunplab-manifest-32021.JPG",
    "/images/icons/apple-icon-180x180-dunplab-manifest-32021.JPG", 
    "/images/icons/favicon-16x16-dunplab-manifest-32021.JPG",
    "/images/icons/favicon-32x32-dunplab-manifest-32021.JPG",
    "/images/icons/favicon-96x96-dunplab-manifest-32021.JPG",
    //IMAGES
    "/images/Accomodatie.jpg",
    "/images/Alexandra.jpg",
    "/images/Demi.jpeg",
    "/images/Interclub.jpg",
    "/images/Logo.JPG",
    "/images/Logo Car Cost Advisor kleur-1.png",
    "/images/logo_gare_de_robe.jpg",
    "/images/logo_Yeti.png",
    "/images/logoYeti.jpg",
    "/images/qrcode.png",
    "/images/rooryck.png",
    "/images/burger.png",
    "/images/rules.jpeg",
    "/images/serruyslogo.jpg",
    "/images/sponsorlogo.png",
    "/images/SportNaschool.png",
    "/images/WPadel.jpg",
    "/assets/media/sort-down.png",
    //VIDEOS
    "/video/indexmovie.mp4",
   // "/video/indexmovie.mp4.filepart",
  //  "/video/teambuilding.mp4",
    //CSS
    "/assets/css/index.css",
    "/assets/css/reset.css",
    "/assets/css/screen.css",
    "/assets/css/academy.css",
    "/assets/css/club.css",
    "/assets/css/contact.css",
    "/assets/css/partners.css",
    "/assets/css/reserveren.css",
    //JS
    "/assets/js/global.js",
    "/assets/js/nav.js",
    "/assets/js/toTopButton.js",
    //FONTS
    "/assets/Fonts/1444649/3d9ec9a9-76a2-4cfe-b376-76e7340c3b50.eot",
    "/assets/Fonts/1444649/7fedd582-6ae3-4850-be2f-4acae2e74fa5.woff",
    "/assets/Fonts/1444649/d6e08ef3-40db-4ac3-82df-f062f55a72f5.ttf",
    "/assets/Fonts/1444649/e0d6f852-5401-4bbf-9672-47a50c5c87c6.woff2",
    "manifest.json",
    "browserconfig.xml"
];

const CACHE_NAME = "PadelbrughiaCache";

self.addEventListener("install", function (event) {
    event.waitUntil(
      caches.open(CACHE_NAME).then(function (cache) {
          return cache.addAll(URLS_TO_CACHE);
      })
    );
  });

  self.addEventListener("fetch", function (event) {
    event.respondWith(fetch(event.request).catch(() => {
     console.log('offline')
      return caches.open(CACHE_NAME).then((cache) => {
          return cache.match(event.request).then((response) => {
          if (response) {
              // Cache hit
              return response;
          } else {
              // Cache miss
              return cache.match(APP_OFFLINE_URL);
          }
          });
      });
      })
    );
  });