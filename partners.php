<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164840027-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164840027-1');
</script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content= "width=device-width, user-scalable=no">
    <link rel="icon" href="images/Logo.JPG" type="image/gif" sizes="20x20">
    <title>Padelbrughia</title>
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/screen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/partners.css">
</head>

<body>
    <header>

        <nav>
            <h1>PadelBrughia</h1>
            <div id="arrowDown" class="closeArrow openArrow rotateimg"></div>
            <div id="popUpNav" class="closeNav openNav">
                <ul>
                    <li><a href="index.html">Startpagina</a></li>
                    <li><a href="club.php">Club</a></li>
                    <li><a href="academy.php">Academy</a></li>
                    <li><a href="reserveren.php">Reserveren</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="partners.php">Partners</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
         <h1> Partners</h1>
        <div id="imgPartners">
            <div id="logoYeti"><i>Yeti Group</i></div>
            <div id="serruyslogo"><i>SERRUYS</i></div>
            <div id="logo_gare_de_robe"><i>Gare De Robe</i></div>
            <div id="sponsorlogo"><i>Zakenkantoor Maene</i></div>
            <div id="burger"><i>Bohemiam Burgers</i></div>
            <div id="Alexandra"><i>Alexandra</i></div>
            <div id="LogoCar_cost_advisor"><i>car cost advisor</i></div> <!--logo nog niet inorde-->
            <div id=""><i></i></div>
        </div>
        <!-- Load Facebook SDK for JavaScript -->
    <!--  <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code --><!--
      <div class="fb-customerchat"
        attribution=install_email
        page_id="1843804475749158"
  logged_in_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ..."
  logged_out_greeting="Hallo! Vind je iets niet? Heb je nog een vraag? Stel ze hier ...">
      </div>-->
    </main>
      <?php
      include('templates/footer.php');
      ?>
</body>
    <div id="webdream">
        <p>Made by Webdream</p>
    </div>
    <script src="assets/js/global.js"></script>
    <script src="assets/js/nav.js"></script>
</html>